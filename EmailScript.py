from openpyxl import load_workbook
import datetime
import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

filepath = "C:\\Users\\zerox\\Documents\\SJ SALES REPORT 2019.xlsx"

wb = load_workbook(filepath, data_only=True)
sheetnames = wb.sheetnames

months = []
for index, month in enumerate(sheetnames, start=0):
    if index == 12:
        break
    months.append(wb[month])
print(months)

weekdays = ['fri', 'sat', 'sun', 'mon', 'tue', 'wed', 'thu']
print(list(enumerate(weekdays, start=0)))

excelData = []
# iterates through months: Jan-Dec, 2019
for month in months[0:12]:
    # iterates through Column B as a tuple ('Tue', 'Wed', 'Thu', 'Fri', etc), one tuple for each month
    for column in month.iter_cols(min_row=5, max_row=39, min_col=1, max_col=3):
        # iterates through each month's tuple, giving value of cells as str objects
        for weekday in column:
            if type(weekday.value) is str and weekday.value.lower() in weekdays:
                date = weekday.offset(column=-1)
                sales = weekday.offset(column=1)
                excelData.append((date.value.date().strftime("%#m-%#d-%Y"), weekday.value, sales.value))

print(excelData)

today = datetime.date.today().strftime("%#m-%#d-%Y")
print(today)

d1 = (datetime.date.today() - datetime.timedelta(days=7)).strftime("%#m/%#d/%y")
print(d1)

d2 = (datetime.date.today() - datetime.timedelta(days=1)).strftime("%#m/%#d/%y")
print(d2)

emailData = []
for i in range(len(excelData)):
    current_tuple = excelData[i]
    if today == current_tuple[0]:
        for j in excelData[i-7:i]:
            emailData.append(j[0] + " $" + str(j[2]))

print(emailData)

smtp_server = "smtp.gmail.com"
port = 587
sender_email = "test458922@gmail.com"
receiver_email = "test458922@gmail.com"
password = input("Please enter your password: ")
message = MIMEMultipart()
message['From'] = sender_email
message['To'] = receiver_email
message['Subject'] = "Minamoto Kitchoan Sales Report" + " " + d1 + "-" + d2

body = "Hello, Mr. Hachimura, Please find the sales report below." + "\n\n" + emailData[0] + "\n" + emailData[1] + "\n"\
 + emailData[2] + "\n" + emailData[3] + "\n" + emailData[4] + "\n" + emailData[5] + "\n" + emailData[6] + "\n\n" + \
 "Thank you very much."

message.attach(MIMEText(body, 'plain'))

context = ssl.create_default_context()

server = smtplib.SMTP(smtp_server, port)
print(server.ehlo())
server.starttls(context=context)
print(server.ehlo())
server.login(sender_email, password)
text = message.as_string()
server.sendmail(sender_email, receiver_email, text)
