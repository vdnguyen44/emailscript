sales = ['friday', 'saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday']
weekdays = enumerate(sales)
print(weekdays)

# Prints tuple value within list named sales
print(sales[1][1])

# prints active workbook page
# wb.active = 3
# print(wb.active)

# enumerate function provides a counter for every element in list, can specify which index to start
weekdays = ['fri', 'sat', 'sun', 'mon', 'tue', 'wed', 'thu']
print(list(enumerate(weekdays)))

# Gives us type of object
print(type("hello world"))

# Gives value for cell object's value in lower case
# jan19 = months[0]['B5']
# print(jan19.value.lower())

# print(wb.sheetnames) gives available sheet titles

# for value in months[0].iter_cols(min_row=5, max_row=39, min_col=2, max_col=2, values_only=True):
    # print(value[0].lower())

x = 5
print(months[x-1])
dates = months[0]['A5:A39']

currentDay = datetime.datetime(2019, 12, 7)
print(currentDay.strftime("%a").lower())

# if type(weekday) is str and weekday.lower() in weekdays:
#     print(weekday)
#         print(index, weekday)
# for index, weekday in enumerate(weekdays, start=0):
#     if type(weekday) is str and weekday in weekdays:
#         print(index, weekday)

for month in months[0:12]:
    # iterates through Column B as a tuple ('Tue', 'Wed', 'Thu', 'Fri', etc), one tuple for each month
    for columnB in month.iter_cols(min_row=5, max_row=39, min_col=1, max_col=3):
        # iterates through each month's tuple, giving value of cells as str objects
        for weekday in columnB:
            if type(weekday.value) is str and weekday.value.lower() in weekdays:
                weekdays2019.append((weekday.cell(column=1).value, weekday.value, weekday.cell(column=3).value))


weekdays2019 = list(enumerate(weekdays2019, start=0))
print(weekdays2019)

# removes zero padding from datetime formatting on windows
mydate.strftime('%#m/%#d/%Y')